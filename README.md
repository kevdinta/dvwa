# Evaluation D25
## Audit SEO et réseau dégradé
L'audit se fera sur le site http://medpharmacom.fr/
Le SEO est déjà assez bon, mais nous pouvons tout de même rappeler les bonnes pratiques du SEO. Pour être bien référencé, il faut que le DOM des différentes pages HTML soit bien structuré. Voici des exemples :
- Il faut avoir une bonne arborescence concernant les balises title (h1, h2...)
- Eviter le duplicate content
- Mettre un minimum de texte avec une description pertinente
- Etre conforme avec la norme W3C
- Intégrer des title et des alt pour les images
- Faire des backlink (Google Juice)
- Eviter de mettre des images très lourdes
- Intégrer un robot.txt pour que les robots puissent indexer les bonnes pages
- Eviter d'utiliser les balises strong pour le côté esthétique, ça ne sert qu'à mettre en valeur un mot

On peut par également, utiliser des images en format webp pour alléger la bande passante également, toutefois, ce n'est malheureusement pas compatible avec certains navigateurs.
Concernant la vitesse de transmission des données on peut également utiliser des CDN, permettant alors de charger les librairies plus rapidement, en effet, grâce aux CDN, les requêtes se font sur le serveur le plus proche du client, ce qui permet alors un temps de chargement plus rapide.
Les images de medpharmacom ne sont pas regroupées, pour optimiser le chargement des pages on peut également utiliser un sprite. C'est un regroupement de plusieurs images en une seule image.
De plus on peut également alléger les fichiers CSS et JS on parle alors de minification, c'est-à-dire, qu'on va chercher à tout compacter le code pour gagner du temps au niveau du loading page.

En utilisant l'outil de Google Chrome pour faire un audit, en affichage Desktop le score est extrêmement faible. On a environ 19 secondes pour le chargement des images, le cache-control est trop bas, il faudrait augmenter la taille du cache.
Cela permet de réduire le temps de chargement des pages, après les avoir visités.
Des fonts sont chargés, mais ne sont pas présentes dans les pages. La connexion n'est pas sécurisée, le protocole HTTPS n'est pas utilisée.

Le temps de chargement pour les réseaux 3G est trop long.

## Faille a) Brute Force
### exploitation
explication + résultats ...
### sécurisation
explication ...
